import { Router, Request, Response, NextFunction } from 'express';

const router = Router();
router.use('/nets', require('./nets/nets'));
module.exports = router;
