export interface Npx {
	readonly E103: string;
	         E201: string;
	readonly E202: string;
}

export interface OrderRequest {
	stan            : string;
	transaction_date: string;
	transaction_time: string;
	amount          : string;
	npx_data        : Npx
}

export interface OrderResponse {
	readonly mti             : string;
	readonly stan            : string;
	readonly transaction_date: string;
	readonly transaction_time: string;
	readonly txn_identifier  : string;
	readonly qr_code         : string;
	readonly response_code   : string;
}

export interface QueryRequest {
	stan            : string;
	transaction_date: string;
	transaction_time: string;
	txn_identifier  : string;
	npx_data        : Npx;
}

export interface QueryResponse {
	readonly stan          : string;
	readonly txn_identifier: string;
	readonly response_code : string;
}