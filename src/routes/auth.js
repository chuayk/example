import   Passport                                         from 'passport'
import   jwt                                              from 'jsonwebtoken'
import { sha256        }                                  from 'hash.js'
import { MailService   }                                  from '@sendgrid/mail'
import { Router       , Request, Response, NextFunction } from 'express'
import { ModelUser     }                                  from '../models/models'
import { flash_message, FlashType }                       from '../helpers/flash-messenger'
import { UserRole      }                                  from '../../build/models/users'

const router = Router();

router.get('/login',                    handle_login);
router.get('/register',                 handle_register);
router.get('/verify/:userId/:token',    handle_verification);

router.post('/login',    handle_login_submit);
router.post('/register', handle_register_submit);

router.all('/logout',    handle_logout);

module.exports = router;

/**
 * 
 * @param {Request} request 
 * @param {Response} response 
 */
function handle_logout(request, response) {
	request.logout();
	response.redirect('/');
}

/**
 * Displays the page for registeration
 * @param {Request} request 
 * @param {Response} response 
 */
function handle_register(request, response) {
	response.render("user/register");
}

/**
 * Handles new user verifications.
 * 1. Check input parameter
 * 2. Fetch specified user
 * 3. Check if already verfied
 * 3.1 If Verified     -> Return Error message (Already verified or something)
 * 3.2 If not Verified -> Verify Token
 * 3.2.1 If Token fail -> Return error
 * 3.2.2 If Token pass -> Return success
 * @param {Request} request 
 * @param {Response} response 
 */
async function handle_verification(request, response) {
	try {
		if (!request.params["userId"] ||
			!request.params["token"]) {
			throw Error("Missing required parameters");
		}
	}
	catch (error) {
		console.error("Bad Request");
		console.error(error);
		return	response.status(400);
	}

	try {
		const user = await ModelUser.findByPk(request.params["userId"]);

		if (user.verified) {
			console.log("User has already been verified");
			flash_message(response, 
				FlashType.Warn, 
				`The activation link has been expired`, 
				"fas fa-sign-in-alt", 
				true);
			response.redirect("/auth/login");
		}
		
		if (jwt.verify(request.params["token"], "P@ssw0rd")) {
			user.update({"verified":  true});

			flash_message(response, 
				FlashType.Success, 
				`${user.email} activated successfully. Please login.`, 
				"fas fa-sign-in-alt", 
				true);
			response.redirect("/auth/login");
		}
		else {
			flash_message(response, 
				FlashType.Error, 
				`Invalid or expired activation link`, 
				"fas fa-sign-in-alt", 
				true);
			response.redirect("/auth/login");
		}
	}
	catch(error) {
		console.error("Unexpected error");
		console.error(error);
		return	response.status(500);
	}
}

/**
 * Handles registeration request
 * @param {Request} request 
 * @param {Response} response 
 */
async function handle_register_submit(request, response) {
	console.log("Incoming Request");
	console.log(request.body);

	//	Check out this link
	// 	https://www.thepolyglotdeveloper.com/2015/05/use-regex-to-test-password-strength-in-javascript/
	//	You can split this for more detailed message but for simplicity
	const fmtPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

	/** 
	 * List of errors.
	 * @type {[{message: string}]} 
	 **/
	const errors = [];
	if (request.body["email"] === undefined)
		errors.push({message: "Email is mandatory"});
	if (request.body["name"] === undefined)
		errors.push({message: "Please provide a name"});
	if (request.body["password"] !== request.body["password2"])
		errors.push({message: "Mismatched password"});

	if (!fmtPassword.test(request.body["password"])) {
		errors.push({message: "Password must contain at least 1 lower, 1 upper, 1 numeric and 1 special character. Minimum size of 8"});
	}


	try {
		if (await ModelUser.count({ where: { "email" : request.body["email"] }}))
			errors.push({message: "This email address have already been used!"});
	}
	catch(error) {
		console.error("Unexpected error");
		console.error(error);
		return	response.status(500);
	}
	
	//	So if got error we just re-draw page with error messages
	if (errors.length > 0) {
		response.render("user/register", {
			"alert_failure": "Unable to register user. See error messages.",
			"errors"       : errors,
			"data"         : request.body
		});
	}
	//	Rediirect to login or you can just show a "Thank you page etc"
	//	Depends on how you want your users to feel
	else {

		try {
			//	Add user to database
			const new_user = await ModelUser.create({
				email   : request.body["email"],
				name    : request.body["name"],
				password: sha256().update(request.body["password"]).digest("hex"),
				role    : UserRole.User
			});
			//	Generate a token
			const token   = jwt.sign(new_user.email, 'P@ssw0rd');
			const link    = `http://localhost:5000/auth/verify/${new_user.uuid}/${token}`;
			const service = new MailService();
			
			service.setApiKey("<Put your API Key here");
			//	Send the activation email
			await service.send({
				to     : new_user.email,
				from   : "admin@beiop.com",
				subject: "Email Verification",
				text   : "Email verification for activating account",
				html   : `Thank you for registering. <br> Please <a href="${link}"><strong>activate</strong></a> your account.`
			});
		}
		catch (error) {
			console.error("Unexpected error");
			console.error(error);
			return	response.status(500);
		}

		flash_message(response, 
			FlashType.Success, 
			`${request.body["email"]} registered successfully. Please activate your account and login.`, 
			"fas fa-sign-in-alt", 
			true);

		return response.render("user/login", {
			//"alert_success": `${request.body["email"]} registered successfully.`
		});
	}
}

/**
 * Displays the page for login
 * @param {Request} request 
 * @param {Response} response 
 */
function handle_login(request, response) {
	response.render("user/login");
}

/**
 * Handles submission of login request
 * @param {Request} request 
 * @param {Response} response 
 * @param {NextFunction}
 */
function handle_login_submit(request, response, next) {
	console.log("Incoming Request");
	console.log(request.body);

	return Passport.authenticate('local', {
		successRedirect: '/video/list',
		failureRedirect: '/auth/login',
		failureFlash   : true
	})(request, response, next);
}