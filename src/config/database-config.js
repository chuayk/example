/**
 * The database configuration used for this application
 */
export const Config = {
	host    : "localhost",
	port    : 3306,
	user    : "itp2155-admin",
	password: "P@ssw0rd",
	database: "itp2155",
};