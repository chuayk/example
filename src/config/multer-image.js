/**
 * @fileoverview 
 * This file contains the implementation configurations for multer to upload image files.
 * Essential steps to work with files
 * CREATE: 
 *	Generate a file path, store file path within database
 * UPDATE: 
 * 	Generate a file path, store file path within database
 *	Delete new file if update failed
 *	Delete old file if update success
 *  If no new file provided, don't do anything to the path or file
 * DELETE: 
 *	Just Delete file
 **/
import   { diskStorage, FileFilterCallback } from 'multer'
import   { v4 as uuidv4 } from 'uuid';
import   Multer  from 'multer'
import   FileSys from 'fs'
import   Path    from 'path'

const local_storage = diskStorage({
	destination: handle_destination,
	filename   : handle_filename
});

/**
 * Regular expression for supported file types
 */
export const FileTypes = /jpeg|jpg|png|gif/;

/**
 * The storage location for images
 */
export const Location = "./public/uploads";

export const Url = "/uploads";

/**
 * File uploading middleware service
 * Import and add this to any route to use
**/
export const FileService = Multer({
	storage   : local_storage,
	fileFilter: filter_file,
	limits    : {
		fileSize: 10485760  // 10MB
	}
});

/**
 * Removes a specified file created by this middleware.
 * @param {string} path The resource path
 */
export function remove_file(path) {
	if (FileSys.existsSync(`./public${path}`))
		FileSys.unlinkSync(`./public${path}`);
	else
		console.warn(`./public/${path} doesn't exists`);
}

/**
 * File destination handler. This handler essentially directs where the file should be stored.
 * @param {Express.Request}         req  The incoming http request
 * @param {Express.Multer.File}     file The incoming file
 * @param {function(Error, string)} callback Multer's handler
 */
function handle_destination(req, file, callback) {
	if (!FileSys.existsSync(Location))
		FileSys.mkdirSync(Location, { recursive: true });

	return callback(null, `${Location}/`);
}

/**
 * Filename handler. This handler essentially generate names for the file to be stored at the destination.
 * @param {Express.Request}         req  The incoming http request
 * @param {Express.Multer.File}     file The incoming file
 * @param {function(Error, string)} callback Multer's handler
**/
function handle_filename(req, file, callback) {
	//return callback(null, `${Path.extname(file.originalname)}`);
	const storage_name = uuidv4();
	console.log(`Saving file as ${storage_name}`);
	return callback(null, storage_name);
}

/**
 * This function assists in filtering applicable files by comparing extension.
 * Take note that this doesn't perform binary validation and is still vulnerable to extension spoofing.
 * @param {Express.Request}     req  The incoming http request
 * @param {Express.Multer.File} file The incoming file
 * @param {FileFilterCallback}  callback Multer's handle
**/
function filter_file(req, file, callback) {
	const ExtName  = FileTypes.test(Path.extname(file.originalname).toLowerCase());
	const MimeType = FileTypes.test(file.mimetype);

	if (ExtName && MimeType) {
		return callback(null, true);
	}
	else {
		callback({"message" : "Invalid image file"});
	}
}